# Relay Compiler
Relay-compiler in a can. Just put it in your Docker-compose file, and leave it to it! 
This image will automatically watch for any changes, and generate the compiled artifacts automatically.

Example Docker-compose.yml:

```yaml
relay-compiler:
    image: meik/relay-compiler
    volumes:
      - .:/app
```


## Advanced usage:
By default, relay-compiler will use the src folder as the directory for the React components, and a schema.graphql file in the root directory for the GraphQL schema. These can be overridden with environment variables:

```yaml
relay-compiler:
    image: meik/relay-compiler
    volumes:
      - .:/app
    environment:
      - SOURCE=custom/src/directory
      - SCHEMA=custom/schema.graphql
```